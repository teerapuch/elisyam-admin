<?php
/**
  * Codeigniter Flashdata Message Library
  * Msg is Message Alert for return text session to bootstrap alert
  * author : Teerapuch Kassakul
  * email : teerapuch@hotmail.com
  * link : https://teerapuch.com
  * version 001
  * date : 14/07/2017
*/
class Msg
{

    static function success()
    {
        $ci =& get_instance();
        if ($ci->session->flashdata('msg-success')) {
            echo '<div class="alert alert-success alert-dismissible" role="alert">';
            echo '<button type="button" class="close" data-dismiss="alert">x</button>';
            echo $ci->session->flashdata('msg-success');
            echo '</div>';
        } // end if
    }

    static function warning()
    {
        $ci =& get_instance();
        if ($ci->session->flashdata('msg-warning')) {
            echo '<div class="alert alert-warning alert-dismissible" role="alert">';
            echo '<button type="button" class="close" data-dismiss="alert">x</button>';
            echo $ci->session->flashdata('msg-warning');
            echo '</div>';
        } // end if
    }

    static function danger()
    {
        $ci =& get_instance();
        if ($ci->session->flashdata('msg-danger')) {
            echo '<div class="alert alert-danger alert-dismissible" role="alert">';
            echo '<button type="button" class="close" data-dismiss="alert">x</button>';
            echo $ci->session->flashdata('msg-danger');
            echo '</div>';
        } // end if
    }

}

?>
